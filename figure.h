#include "color.h"
// Класс фигуры (точка, линия, треугольник), метод для передачи фигуры в пнг
// class Figures
// {
//    public:
//       virtual void DrawAlg(uint32_t** frame_buffer, int width, int height, Point2D coodr_centre, RGB color )  = 0;
//       virtual void DrawAlg(func_ptr set_point, int width, int height, Point2D coodr_centre, RGB color )  = 0;

//       virtual void DrawShape() = 0;
//       virtual void ReadShape() = 0;
// };

// Color::RED.GetRGBA();

class Point2D
{
   public:
      explicit Point2D(const int& x, int y):x_(x), y_(y){};
      Point2D(){};
      int x(){return x_;}
      void set_x(int x){x_ = x;}

      int y(){return y_;}
      void set_y(int y){y_ = y;}

      Color color() const {return color_;}
      void set_color(Color color){color_ = color;}
   private:
      int x_, y_;
      Color color_;
};

class Point3D
{
   public:
      explicit Point3D(const float& x, float y, float z):x_(x), y_(y), z_(z){};
      Point3D(){};
      float x(){return x_;}
      void set_x(int x){x_ = x;}

      float y(){return y_;}
      void set_y(int y){y_ = y;}

      float z(){return z_;}
      void set_z(int z){z_ = z;}

      Color color() const {return color_;}
      void set_color(Color color){color_ = color;}

      void normalize_coordinates(int max_x, int max_y)
      {
         x_ = x_ / max_x;
         y_ = y_ / max_y;
      };

      Point2D switch_to_pixel(int canvas_length, int canvas_width)
      {
         Point2D point;
         // not sure about these:
         // point.set_x((( x_/(z_) + 1 )/2 * canvas_width));
         // point.set_y((( y_/(z_) + 1 )/2 * canvas_length));
         point.set_x( x_/(z_));
         point.set_y( y_/(z_));
         return point;
      };

   private:
      float x_, y_, z_;
      Color color_;
};

class Line3D
{
   public:
      void Rotate(double degree);
      Line3D(){};
      Line3D(Point3D point_1, Point3D point_2):
      point_1_(point_1),
      point_2_(point_2){
         Point3D centre;
         centre.set_x( (point_1_.x() + point_2_.x()) / 2 );
         centre.set_y( (point_1_.y() + point_2_.y()) / 2 );
         centre.set_z( (point_1_.z() + point_2_.z()) / 2 );
      };
      Line3D(Point2D point_1, Point2D point_2):
      point_01_(point_1),
      point_02_(point_2){};
      // Point2D point_1(){return point_01_;}
      // Point2D point_2(){return point_02_;}
      // Point2D centre(){return centre_.switch_to_pixel();}
      Point3D point_1(){return point_1_;}
      Point3D point_2(){return point_2_;}
      Point3D centre(){return centre_;}
      Color color() const {return color_;}
      void set_color(Color color){color_ = color;}
   private:
      Point3D point_1_, point_2_, centre_;
      Point2D point_01_, point_02_;
      Color color_;
};

class Triangle3D
{
   public:
      Triangle3D(Point3D point_1, Point3D point_2, Point3D point_3):
      point_1_(point_1),
      point_2_(point_2),
      point_3_(point_3){
         Point3D centre;
         centre.set_x((point_1.x() + point_2.x() + point_3.x())/3);
         centre.set_y((point_1.y() + point_2.y() + point_3.y())/3);
         centre.set_z((point_1.z() + point_2.z() + point_3.z())/3);
         // float y_delta_1 = point_2.y() - point_1.y();
         // float x_delta_1 = point_2.x() - point_1.x();
         // float y_delta_2 = point_3.y() - point_2.y();
         // float x_delta_2 = point_3.x() - point_2.x();
         // float slope_1 = y_delta_1/x_delta_1;
         // float slope_2 = y_delta_2/x_delta_2;  
         // centre_.set_x( slope_1 * slope_2 * (point_1.y() - point_3.y()) + slope_2 * (point_1.x() + point_2.x()) - slope_1 * (point_1.x() + point_3.x())/(2* (slope_2 - slope_1) ));
         // centre_.set_y( -1*(centre.x() - (point_1.x() + point_2.x())/2)/slope_1 +  (point_1.y() + point_2.y())/2 );
      };
      Triangle3D(Point2D point_1, Point2D point_2, Point2D point_3):
      point_01_(point_1),
      point_02_(point_2),
      point_03_(point_3){};
      Point3D point_1(){return point_1_;}
      Point3D point_2(){return point_2_;}
      Point3D point_3(){return point_3_;}
      Point3D centre(){return centre_;}
      Color color() const {return color_;}
      void set_color(Color color){color_ = color;}
      void move_to(Point3D render_cnt)
      {
         int x_shift = -(centre_.x() - render_cnt.x());
         // cout << x_shift << endl;
         int y_shift = -(centre_.y() - render_cnt.y());
         // cout << y_shift << endl;
         int z_shift = -(centre_.z() - render_cnt.z());
         // cout << z_shift << endl;
         point_1_.set_x(point_1_.x() + x_shift);
         point_1_.set_y(point_1_.y() + y_shift);
         point_1_.set_z(point_1_.z() + z_shift);
         point_2_.set_x(point_2_.x() + x_shift);
         point_2_.set_y(point_2_.y() + y_shift);
         point_2_.set_z(point_2_.z() + z_shift);
         point_3_.set_x(point_3_.x() + x_shift);
         point_3_.set_y(point_3_.y() + y_shift);
         point_3_.set_z(point_3_.z() + z_shift);
      };
   private:
      Point3D point_1_, point_2_, point_3_, centre_;
      Point2D point_01_, point_02_, point_03_;
      Color color_;

};