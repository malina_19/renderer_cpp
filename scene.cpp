#define RGBA(r, g, b, a) ((r) | ((g) << 8) | ((b) << 16) | ((a) << 24))
#include "scene.h"

Scene::Scene(int len, int width, float ang):
length_(len),
width_(width),
angle_(ang){
    // std::cout << "length_ " <<  length_ << std::endl;
    // std::cout << "width_" << width_ << std::endl;
    arr_ = new uint32_t*[length_];
    for (int i = 0; i < length_; i++)
    {
        arr_[i] = new uint32_t[width_];
    }
    for (int i = 0; i < length_; i++)
    {
        for (int j = 0; j < width_; j++)
        {
            arr_[i][j] = 0;
        }
    }
    z_ = length_/(2*tan(angle_/2));
}

Scene::~Scene(){
    for (int i = 0; i < length_; i++)
    {
        delete [] arr_[i];
    }
    delete [] arr_;
}

void Scene::dump(const char* png_file_name)
{
    if (arr_ == nullptr)
    {
        std::cout << "arr_ not allocated" << std::endl;
    }
    libattopng_t *png = libattopng_new(length_, width_, PNG_RGBA);
    int x, y;
    for (y = 0; y < length_; y++)
    {
        for (x = 0; x < width_; x++)
        {
            // if ( (arr_[x][y] != Color(MIN_COLOR,MIN_COLOR,MIN_COLOR,MIN_TRANSPARENCY) ) )
            // {
            //     libattopng_set_pixel(png, x, y, RGBA(0xff, 0, 0, 0xff));
            // }
            // else
            {
                libattopng_set_pixel(png, x, y, arr_[x][y]);
            }
        }
    }
    libattopng_save(png, png_file_name);
    libattopng_destroy(png);
}

void Scene::draw_line(Point2D point_1, Point2D point_2, Color color)
{
    int dx =  abs (point_2.x() - point_1.x()), sx = point_1.x() < point_2.x() ? 1 : -1;
    int dy = -abs (point_2.y() - point_1.y()), sy = point_1.y() < point_2.y() ? 1 : -1; 
    int err = dx + dy;
    int e2;
    for (;;)
    {  
        put(point_1, color);
        if (point_1.x() == point_2.x() && point_1.y() == point_2.y() )
        {
            put(point_2, color);
            break;
        }
        e2 = 2 * err;
        if (e2 >= dy) { err += dy; point_1.set_x( point_1.x() + sx ); }
        if (e2 <= dx) { err += dx; point_1.set_y( point_1.y() + sy ); }
    }
}

void Scene::fill_bottom_flat_triangle(Point2D point_1, Point2D point_2, Point2D point_3, Color color)
{        
    float inv_slope_1 = (float)(point_2.x() - point_1.x()) / (float)(point_2.y() - point_1.y());
    float inv_slope_2 = (float)(point_3.x() - point_1.x()) / (float)(point_3.y() - point_1.y());
    float cur_x_1 = point_1.x();
    float cur_x_2 = point_1.x();
    for (int scan_line_y = point_1.y(); scan_line_y <= point_2.y(); scan_line_y++)
    {
        Point2D p1(cur_x_1, scan_line_y);
        Point2D p2(cur_x_2, scan_line_y);
        draw_line(p1, p2, color);
        cur_x_1 += inv_slope_1;
        cur_x_2 += inv_slope_2;
    }
}   

void Scene::fill_top_flat_triangle(Point2D point_1, Point2D point_2, Point2D point_3, Color color)
{
    float inv_slope_3 = (float)(point_3.x() - point_1.x()) / (float)(point_3.y() - point_1.y());
    float inv_slope_4 = (float)(point_3.x() - point_2.x()) / (float)(point_3.y() - point_2.y());

    float cur_x_3 = point_3.x();
    float cur_x_4 = point_3.x();

    for (int scan_line_y_1 = point_3.y(); scan_line_y_1 > point_1.y(); scan_line_y_1--)
    {
        Point2D p1((int)cur_x_3, scan_line_y_1);
        Point2D p2((int)cur_x_4, scan_line_y_1);
        draw_line(p1, p2, color);
        cur_x_3 -= inv_slope_3;
        cur_x_4 -= inv_slope_4;
    }
}

    // sort the three vertices by y-coordinate ascending so a is the topmost vertice
void Scene::sort_points(Point2D a, Point2D b, Point2D c, Point2D *max, Point2D *mid, Point2D *min) 
{
    vector< pair <int, int> > vect;
    int arr[] = {a.x(), b.x(), c.x()};
    int arr1[] = {a.y(), b.y(), c.y()};
    int n = sizeof(arr)/sizeof(arr[0]);
    for (int i=0; i<n; i++)
    vect.push_back( make_pair(arr[i],arr1[i]) );
    auto sort_by_y = [&](const pair<int,int> &a, const pair<int,int> &b)
    {
        return (a.second < b.second);
    };
    sort(vect.begin(), vect.end(), sort_by_y);
    max->set_x(vect[0].first);
    max->set_y(vect[0].second);
    mid->set_x(vect[1].first);
    mid->set_y(vect[1].second);
    min->set_x(vect[2].first);
    min->set_y(vect[2].second);
}

void Scene::put(Point3D point, Color color)
{
    Point2D pixel = point.switch_to_pixel(length_, width_);
    // std::cout << "pixel.x()=" << pixel.x() << endl;
    // std::cout << "pixel.y()=" << pixel.y() << endl;
    if ((pixel.x() >= 0) && (pixel.x() <= length_) && (pixel.y() >= 0) && (pixel.y() <= width_))
    {
        arr_[(int)pixel.x()][(int)pixel.y()] = color.get_RGBA();

    }
    else
    {
        std::cout << "Error occuried: wrong coordinates" << endl;
    }
}

void Scene::put(Point2D point, Color color)
{
    if ((point.x() >= 0) && (point.x() <= length_) && (point.y() >= 0) && (point.y() <= width_))
    {
        arr_[point.x()][point.y()] = color.get_RGBA();

    }
    else
    {
        std::cout << "pixel.x()=" << point.x() << endl;
        std::cout << "pixel.y()=" << point.y() << endl;
        std::cout << "Error occuried: wrong coordinates" << endl;
    }
}

void Scene::put(Line3D line, Point3D centre, Color color)
{
    // std::cout << "line.point_1.x()=" << line.point_1().x() << endl;
    // std::cout << "line.point_1.y()=" << line.point_1().y() << endl;
    // std::cout << "line.point_2.x()=" << line.point_2().x() << endl;
    // std::cout << "line.point_2.y()=" << line.point_2().y() << endl;
    // std::cout << "__________________________________________" << endl;
    // Line3D line_1;
    // Point2D pixel_1, pixel_2;
    // pixel_1 = line.point_1().switch_to_pixel(length_, width_);
    // pixel_2 = line.point_2().switch_to_pixel(length_, width_);
    // line_1.point_1().set_x( pixel_1.x() );
    // line_1.point_1().set_y( pixel_1.y() );
    // line_1.point_2().set_x( pixel_2.x() );
    // line_1.point_2().set_y( pixel_2.y() );
    // std::cout << "line_1.point_1.x()=" << line_1.point_1().x() << endl;
    // std::cout << "line_1.point_1.y()=" << line_1.point_1().y() << endl;
    // std::cout << "line_1.point_2.x()=" << line_1.point_2().x() << endl;
    // std::cout << "line_1.point_2.y()=" << line_1.point_2().y() << endl;
    // std::cout << "__________________________________________" << endl;
    Point2D pixel_1, pixel_2;
    pixel_1 = line.point_1().switch_to_pixel(length_, width_);
    pixel_2 = line.point_2().switch_to_pixel(length_, width_);
    draw_line(pixel_1, pixel_2, color);
}

void Scene::put(Triangle3D triangle, Point3D centre, Color color)
{
    Point2D pixel_1, pixel_2, pixel_3, pixel_4;
    pixel_1 = triangle.point_1().switch_to_pixel(length_, width_);
    pixel_2 = triangle.point_2().switch_to_pixel(length_, width_);
    pixel_3 = triangle.point_3().switch_to_pixel(length_, width_); 
    // point_t_1 = point_1;
    // point_t_2 = point_2;
    // point_t_3 = point_3;
    // Point2D point_t_1, point_t_2, point_t_3, point_4;
    sort_points(pixel_1, pixel_2, pixel_3, &pixel_1, &pixel_2, &pixel_3);
    // sort(point_t_1, point_t_2, point_t_3, &point_t_1, &point_t_2, &point_t_3);

    if (pixel_2.y() == pixel_3.y())
    {
        fill_bottom_flat_triangle(pixel_1, pixel_2, pixel_3, color);
    }
    else 
    {
        if (pixel_1.y() == pixel_2.y())
        {
            fill_top_flat_triangle(pixel_1, pixel_2, pixel_3, color);
        }
        else
        {
        /* general case - split the triangle in a topflat and bottom-flat one */
            pixel_4.set_x( pixel_1.x() + (float)((pixel_2.y() - pixel_1.y()) / (float)(pixel_3.y() - pixel_1.y())) * (float)(pixel_3.x() - pixel_1.x()) );
            pixel_4.set_y(pixel_2.y());
            fill_bottom_flat_triangle(pixel_1, pixel_4, pixel_2, color);
            fill_top_flat_triangle(pixel_2, pixel_4, pixel_3, color);
        }
    }
}

