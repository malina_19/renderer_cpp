#define RGBA(r, g, b, a) ((r) | ((g) << 8) | ((b) << 16) | ((a) << 24))
#include <iostream>

using namespace std;

constexpr double MAX_COLOR = 1.0;
constexpr double MIN_COLOR = 0;
// max transparency at 0%
constexpr double MAX_TRANSPARENCY = 0;
// min transparency at 100%
constexpr double MIN_TRANSPARENCY = 1.0;

class Color
{
   public:
      Color(double r, double g, double b, double a):red_(r), green_(g), blue_(b), transparency_(a){};
      Color():red_(MIN_COLOR), green_(MIN_COLOR), blue_(MIN_COLOR), transparency_(MIN_TRANSPARENCY){};
      uint32_t get_RGBA()
      { 
        uint32_t uint_color;  
        return uint_color = (uint32_t)( (uint32_t)red_ * 255 | (uint32_t)green_ * 255 << 8 | (uint32_t)blue_ * 255 << 16 | (uint32_t)transparency_ * 255 << 24 ); 
      }
      static Color RED(){return Color(MAX_COLOR,MIN_COLOR,MIN_COLOR,MIN_TRANSPARENCY);}

   private:
      double red_ = MIN_COLOR;
      double green_ = MIN_COLOR;
      double blue_ = MIN_COLOR;
      double transparency_ = MIN_TRANSPARENCY;
};