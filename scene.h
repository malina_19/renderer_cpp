#include <iostream>
#include <array>
#include <fstream>
#include <string>
#include <vector>
#include<bits/stdc++.h>
#include "figure.h"
extern "C"
{
  #include "libattopng.h"
}

class Scene
{
  public:
      Scene(int len = 255, int width = 255, float ang = 90);
      ~Scene();
      void dump(const char* png_file_name);
      void draw_line(Point2D point_1, Point2D point_2, Color color);
      void fill_bottom_flat_triangle(Point2D point_1, Point2D point_2, Point2D point_3, Color color);
      void fill_top_flat_triangle(Point2D point_1, Point2D point_2, Point2D point_3, Color color);
      void sort_points(Point2D a, Point2D b, Point2D c, Point2D *max, Point2D *mid, Point2D *min);

      void put(Point3D point, Color color);
      void put(Point2D point, Color color);
      void put(Line3D line, Point3D centre, Color color);
      void put(Triangle3D triangle, Point3D centre, Color color);
      // void put(Circle circle, Point2D centre);
      // void put(Model model, Point2D centre);

  private:
      int length_ = 0;
      int width_ = 0;
      float angle_ = 0;
      float z_ = 1;
      uint32_t** arr_ = nullptr;
};


