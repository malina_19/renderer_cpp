#include "scene.h"


int main() 
{
    // Point2D p;
    // p.SetX(4);
    // p.SetY(80);
    // Png();
    Scene sc(200, 200, 90);
    // sc.PutPoint(p);
    // // p.~Png();
    Point3D p1(4, 100, 2);
    Point3D p2(30, 50, 3);
    Point3D p3(80, 60, 5);
    Line3D l1(p1, p2);
    Line3D l2(p3, p2);
    Line3D l3(p3, p1);
    // Point2D ren_cnt(100, 100);
    Triangle3D tr(p1, p2, p3);
    // tr.move_to(ren_cnt);
    sc.put(l1, l1.centre(), Color(0, 1, 0, 1));
    sc.put(l2, l2.centre(), Color(0, 1, 0, 1));
    sc.put(l3, l3.centre(), Color(0, 1, 0, 1));
    sc.put(p1, Color(1, 1, 0, 1));
    sc.put(p2, Color(1, 1, 0, 1));
    sc.put(p3, Color(1, 1, 0, 1));
    // Point2D cnt = tr.centre();
    // Triangle2D tr1(p1, cnt, p2);
    // Triangle2D tr2(p1, cnt, p3);
    // Triangle2D tr3(p2, cnt, p3);
    
    sc.put(tr, tr.centre(), Color(1, 0, 0, 1));
    // sc.put(tr2, tr2.centre(), Color(1, 1, 0, 1));
    // sc.put(tr3, tr3.centre(), Color(1, 0, 1, 1));
    
    sc.dump("pic.png");
    return 0;

}

