#include "gtest/gtest.h"
extern "C"
{
  #include "../libattopng.h"
}

TEST(example_1, ok) 
{
  auto is_pic_dupmt = [=]()
  {
    int x, y;
    int** arr;
    arr = new int*[11];
    for (int i = 0; i < 11; i++)
    {
        arr[i] = new int[11];
    }
    libattopng_t *png = libattopng_new(10, 10, PNG_RGBA);
    bool flag = false;
    for (y = 0; y < 10; y++)
    {
      for (x = 0; x < 10; x++)
      {
          libattopng_set_pixel(png, x, y, arr[x][y]);
      }
    }
    libattopng_save(png, "pic.png");
    flag = true;
    for (int i = 0; i < 11; i++)
    {
        delete [] arr[i];
    }
    delete [] arr;
    return flag;
  };
  ASSERT_TRUE(is_pic_dupmt());
}

// TEST(example_2, ok) {
//   ASSERT_EQ(test_return_5(), 4);
// }